// open browser and preview img

var loadFile = function (event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src) // free memory
    }

    preview.style.border = "none";
    iconImg.hidden = true;
    output.style.width = "250px"
    output.style.height = "250px"

};

function checkFilled() {
    var inputVal = document.getElementById("groupName");
    if (inputVal.value == "") {
        inputVal.style.borderColor = "red";
    } else{
        inputVal.style.borderColor = "";
    }

    var category = document.getElementById("category");
    if (category.value == "") {
        category.style.borderColor = "red"

        // how to display only red color that show on input field, not change options??
        category.style.color = "red"
    } else {
        category.style.borderColor = "";
        category.style.color = "";
    }
}
